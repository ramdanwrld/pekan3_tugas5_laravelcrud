<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="first_nama"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="last_nama"><br><br>

        <label>Gender</label><br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>

        <label>Nationality:</label><br><br>
        <select name="Nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Indonesian">Singaporean</option>
            <option value="Indonesian">Malaysian</option>
            <option value="Indonesian">Australian</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="Bahasa Indonesia" value="Bahasa Indonesia">
        <label for="bahasaIndonesia"> Bahasa Indonesia</label><br>
        <input type="checkbox" name="English" value="English">
        <label for="english"> English</label><br>
        <input type="checkbox" name="Other" value="Other">
        <label for="other"> Other</label><br><br>

        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit">
    </form>
  </body>
</html>
